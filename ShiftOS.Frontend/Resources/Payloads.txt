﻿/* Plex Payloads data file
 *
 * This file contains information about all payloads in the game's campaign.
 *
 */

[
	{
		FriendlyName: "FTP File Puller Payload",
		PayloadName: "ftpull",
		Port: 21,
		Function: 2
	},
	{
		FriendlyName: "Reverse shell",
		PayloadName: "rsh",
		Port: 22
	},
	/* This is for when we implement the ability to watch players you hack in Multiplayer.
	{
		FriendlyName: "Plexnet VNC viewer",
		PayloadName: "pvnc",
		Port: 5900
	}*/
]